from django.urls import path
from app import views

urlpatterns = [
    path('', views.home,  name="home"),
    path('about/', views.about, name="about"),
    path('experience/', views.experience, name="experience"),
    path('registration/', views.registration, name = "registration")
]
